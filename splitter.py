#!/usr/bin/env python

import numpy as np
import cv2
import datetime
import os
import sys
import subprocess
import collections

print("Found OpenCV %s" % cv2.__version__)
cv2.setUseOptimized(True)
print("Default OpenCV Optimizations: %s" % str(cv2.useOptimized()))


class Source:
    def __init__(self, files):
        self.files = files
        self.idx = 0
        self.file = self.files[self.idx]
        self.frame_idx = 0
        self.previous_file_length = 0
        self.start_fragment = None
        self.end_fragment = None
        self.nextFile()

    def nextFile(self):
        self.cap = cv2.VideoCapture(self.file)
        while not self.cap.isOpened():
            self.idx += 1
            if self.idx >= len(self.files):
                return
            self.file = self.files[self.idx]
            self.cap.release()
            self.cap = cv2.VideoCapture(self.file)
            self.frame_idx = 0

    def clean(self):
        self.previous_file_length = 0
        self.start_fragment = None
        self.end_fragment = None

    def release(self):
        self.cap.release()

    def start(self):
        self.start_fragment = (self.frame_idx, self.file)

    def end(self):
        self.end_fragment = (self.frame_idx, self.file)

    def next(self):
        ret, frame = self.cap.read()
        if ret:
            self.frame_idx += 1
            return ret, frame

        self.idx += 1
        if self.idx >= len(self.files):
            return
        self.file = self.files[self.idx]
        self.previous_file_length = self.frame_idx
        self.nextFile()
        return self.next()

    def hasNext(self):
        return self.idx < len(self.files) or self.cap and self.cap.isOpened()

EXTS = [".mts", ".mp4", ".mov"]
folder = "."

if len(sys.argv) > 1:
    folder = sys.argv[1]

onlyfiles = [os.path.join(folder, f) for f in os.listdir(folder)
             if os.path.isfile(os.path.join(folder, f)) and
             os.path.splitext(f)[1].lower() in EXTS]

onlyfiles.sort()

if not onlyfiles:
    print "No video files found in %s" % folder
    exit(-1)

extension = os.path.splitext(onlyfiles[0])[1]
onlyfiles = filter(lambda a: a.lower().endswith(extension.lower()), onlyfiles)
source = Source(onlyfiles)

print "Processing %d files in %s" % (len(onlyfiles), folder)


def visualizeColorPlane(hsv):
    hist_scale = cv2.getTrackbarPos('scale', 'hsv_map')
    h = cv2.calcHist([hsv], [0, 1], None, [180, 256], [0, 180, 0, 256])
    h = np.clip(h * 0.005 * hist_scale, 0, 1)
    vis = hsv_map * h[:, :, np.newaxis] / 255.0
    cv2.imshow('hsv_map', vis)


def ffmpeg_check():
    try:
        cmd = ["ffmpeg", "-h", "-loglevel", "error"]
        subprocess.check_output(cmd)
        return True
    except:
        return False


def ffmpeg_split(filename, start, end, output_filename):
    cmd = ["ffmpeg", "-y", "-i", filename, "-ss", str(start), "-t", str(end),
           "-vcodec", "copy", "-acodec", "copy", "-loglevel", "error",
           output_filename]
    print(">>> %s" % " ".join(cmd))
    subprocess.check_output(cmd)


def ffmpeg_merge(files, output_filename):
    cmd = ["ffmpeg", "-y", "-i", "concat:%s" % "|".join(files),
           "-vcodec", "copy", "-acodec", "copy", "-loglevel", "error",
           output_filename]
    print(">>> %s" % " ".join(cmd))
    subprocess.check_output(cmd)


def saveClip(source, file_idx):
    output_filename = os.path.join(folder, 'clip-%d%s' % (file_idx, extension))

    if source.start_fragment[1] == source.end_fragment[1]:
        # Same file
        start = source.start_fragment[0] / fps
        end = source.end_fragment[0] / fps

        ffmpeg_split(source.file, start, end - start, output_filename)
    else:
        start = source.start_fragment[0] / fps
        end = source.previous_file_length / fps

        output_file_1 = 'clip-%d-v1.mts' % file_idx
        ffmpeg_split(source.files[source.idx - 1],
                     start,
                     end - start,
                     output_file_1)

        start = 0
        end = source.end_fragment[0] / fps
        # Same file
        output_file_2 = 'clip-%d-v2.mts' % file_idx
        ffmpeg_split(source.file, start, end - start, output_file_2)

        ffmpeg_merge([output_file_1, output_file_2], output_filename)

        os.remove(output_file_1)
        os.remove(output_file_2)
cv2.namedWindow('frame', 1)

kernel_dilate = np.ones((15, 15), np.uint8)
kernel_erode = np.ones((5, 5), np.uint8)
disc = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))

if not source.hasNext():
    print "No supported files was found"
    exit(0)

width = int(source.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(source.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
fps = source.cap.get(cv2.CAP_PROP_FPS)

cv2.createTrackbar('Area', 'frame', 3000, 25000, lambda a: a)
cv2.createTrackbar('Score', 'frame', 90, 100, lambda a: a)

show_hsv_debug = False
hsv_map = None
if show_hsv_debug:
    hsv_map = np.zeros((180, 256, 3), np.uint8)
    h, s = np.indices(hsv_map.shape[:2])
    hsv_map[:, :, 0] = h
    hsv_map[:, :, 1] = s
    hsv_map[:, :, 2] = 255
    hsv_map = cv2.cvtColor(hsv_map, cv2.COLOR_HSV2BGR)

    cv2.namedWindow('hsv_map', 0)
    cv2.createTrackbar('scale', 'hsv_map', 10, 32, lambda a: a)

# Scale image to 800x*
scale = 600.0 / width

background_hist = np.zeros((180, 256), np.uint8)

calibrating = True
calibrating_until_frame = 100


empty_spot_timeout_seconds = 3
history = np.ones((int(fps * empty_spot_timeout_seconds)), np.uint8)
file_idx = 1

fps_count = collections.deque(maxlen=int(fps))


def on_mouse(event, x, y, flags, params):
    global calibrating, background_hist
    if event == cv2.EVENT_LBUTTONDOWN:
        calibrating = True
        background_hist = np.zeros((180, 256), np.uint8)
    elif event == cv2.EVENT_LBUTTONUP:
        calibrating = False

cv2.setMouseCallback('frame', on_mouse)

if not ffmpeg_check():
    print "FFmpeg executable not found. Please install FFmpeg and put it " + \
        "to PATH variable"
    exit(-1)

while(True):
    ret, frame = source.next()

    if not ret:
        break

    t0 = cv2.getTickCount()

    # 1. Extract ROI
    # TODO: Allow user to setup ROI

    # 2. Reduce image size to increase performance
    roi = cv2.resize(frame, (int(scale * width), int(scale * height)))

    # 3. Blur image to decrease contrast
    blur = cv2.blur(roi, (16, 16))

    # 4. Adjust image histogram to compensate environment change
#    for c in [0, 1, 2]:
#        blur[:, :, c] = cv2.equalizeHist(blur[:, :, c])

    # 5. Transform to HSV color space
    hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)

    # Show color pane for debug purposes
    if show_hsv_debug:
        visualizeColorPlane(hsv)

    # 6. Calibrate background histogram
    if calibrating:
        hist = cv2.calcHist([hsv], [0, 1], None, [180, 256], [0, 180, 0, 256])
        background_hist = background_hist + hist
        if source.frame_idx == calibrating_until_frame and calibrating:
            calibrating = False

    # 7. Remove background
    dst = cv2.calcBackProject([hsv],
                              [0, 1],
                              background_hist,
                              [0, 180, 0, 256],
                              1)

    cv2.filter2D(dst, -1, disc, dst)
    ret, thresh = cv2.threshold(dst, 50, 255, 0)
    thresh = cv2.merge((thresh, thresh, thresh))
    fg = cv2.bitwise_and(roi, cv2.bitwise_not(thresh))
    hsv = cv2.cvtColor(fg, cv2.COLOR_BGR2HSV)

    # 8. Extract features based on color anomaly
    lower = np.array([0, 60, 1])
    upper = np.array([255, 255, 255])
    mask = cv2.inRange(hsv, lower, upper)
    mask = cv2.dilate(mask, kernel_dilate, iterations=1)
    mask = cv2.erode(mask, kernel_erode, iterations=1)

    # 9. Find contours on extracted features
    mask, contours, hierarchy = cv2.findContours(mask,
                                                 cv2.RETR_LIST,
                                                 cv2.CHAIN_APPROX_SIMPLE)

    render = roi

    empty_spot_detected = True
    area = 0
    area_threshold = cv2.getTrackbarPos('Area', 'frame')
    score_threshold = cv2.getTrackbarPos('Score', 'frame') / 100.0
    if contours:
        for cnt in contours:
            area += cv2.contourArea(cnt)

        empty_spot_detected = area < area_threshold

    history = np.hstack(([empty_spot_detected], history[:-1]))
    if calibrating:
        cv2.putText(render,
                    "CALIBRATING",
                    (5, 40),
                    cv2.FONT_HERSHEY_COMPLEX_SMALL,
                    1,
                    (0, 255, 255))
    elif empty_spot_detected:
        cv2.putText(render,
                    "EMPTY",
                    (5, 40),
                    cv2.FONT_HERSHEY_COMPLEX_SMALL,
                    1,
                    (0, 255, 0))
    else:
        cv2.putText(render,
                    "OCCUPIED",
                    (5, 40),
                    cv2.FONT_HERSHEY_COMPLEX_SMALL,
                    1,
                    (0, 0, 255))

    if (np.average(history) > score_threshold):
        cv2.putText(render,
                    "CUTTTING",
                    (40, 200),
                    cv2.FONT_HERSHEY_COMPLEX_SMALL,
                    3,
                    (0, 0, 255))
        if source.start_fragment:
            source.end()

            saveClip(source, file_idx)
            file_idx += 1

            source.clean()
    else:
        if not source.start_fragment:
            source.start()

    t1 = cv2.getTickCount()
    fps_count.append(1 / ((t1 - t0) / cv2.getTickFrequency()))
    ts = datetime.timedelta(seconds=int(source.frame_idx / fps))
    cv2.putText(render, "%s (%03d FPS), Area: %05d, "
                        "Score: %02d %s" % (ts,
                                            int(np.average(fps_count)),
                                            int(area),
                                            np.average(history) * 100,
                                            source.file),
                        (5, 20),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL,
                        0.7,
                        (225, 255, 255))

    cv2.putText(render, "Press 'ESC' to exit",
                        (5, int(height * scale) - 15),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL,
                        0.7,
                        (225, 255, 255))

    cv2.imshow('frame', render)

    key = cv2.waitKey(1) & 0xFF
    if key == 27:
        break

if source.start_fragment:
    source.end()
    saveClip(source, file_idx)

source.release()
cv2.destroyAllWindows()
